package com.demo.javabeandemo.mainapp;

import com.demo.javabeandemo.config.HibernateConfig;
import com.demo.javabeandemo.pojo.Teacher;
import org.hibernate.Session;

public class MainApp4 {
    public static void main(String[] args) {
        Session session= HibernateConfig.getSessionFactory().openSession();

        Teacher teacher= new Teacher();
        teacher.setTeacherId(2);
        teacher.setTeacherName("Amitabh bachhan");
        teacher.setSubject("computer science");

        session.update(teacher);
        session.beginTransaction().commit();
        session.close();

    }
}
