package com.demo.javabeandemo.mainapp;

import com.demo.javabeandemo.config.HibernateConfig;
import com.demo.javabeandemo.pojo.Teacher;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class MainApp1 {
    public static void main(String[] args) {

        //building a session

        Session session= HibernateConfig.getSessionFactory().openSession();

        Teacher teacher = new Teacher();
        teacher.setSubject("computer science");
        teacher.setTeacherName("rakesh kumar bacchan");

        session.save(teacher);  //saves the teacher object to the database
        session.beginTransaction().commit();
        session.close();


    }
}
