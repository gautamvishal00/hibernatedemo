package com.demo.javabeandemo.mainapp;

import com.demo.javabeandemo.config.HibernateConfig;
import com.demo.javabeandemo.pojo.Teacher;
import org.hibernate.Session;

public class MainApp3 {
    public static void main(String[] args) {
        Session session= HibernateConfig.getSessionFactory().openSession();

        Teacher teacher= new Teacher();
        teacher.setTeacherId(1);

        session.delete(teacher);
        System.out.println("Successfully deleted");
        session.beginTransaction().commit();
        session.close();
    }
}
