package com.demo.javabeandemo.mainapp;


import com.demo.javabeandemo.config.HibernateConfig;
import com.demo.javabeandemo.pojo.Teacher;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class MainApp2 {
    public static void main(String[] args) {
        Session session= HibernateConfig.getSessionFactory().openSession();

        List<Teacher> teacherList=session.createCriteria(Teacher.class).list();
        session.close();

        ///list of teachers from school

        for(Teacher t:teacherList){
            System.out.println(t);
        }
    }
}
