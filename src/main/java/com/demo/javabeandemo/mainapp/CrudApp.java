package com.demo.javabeandemo.mainapp;

import com.demo.javabeandemo.pojo.Teacher;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class CrudApp {
    private SessionFactory sessionFactory;
    CrudApp(){
        Configuration configuration= new Configuration().configure("./hibernate.cfg.xml");
        SessionFactory sessionFactory=configuration.buildSessionFactory();
        this.sessionFactory=sessionFactory;
    }
    public void delete(){
        Teacher teacher= new Teacher();
        teacher.setTeacherId(3);
        Session session=sessionFactory.openSession();
        session.delete(teacher);
        System.out.println("deleted");
        session.beginTransaction().commit();
        session.close();
    }
    public List<Teacher> getAllTeachers(){
        Session session= sessionFactory.openSession();
        List<Teacher> teacherList=session.createCriteria(Teacher.class).list();
        session.close();
        return teacherList;
    }
    public void update(){
        Teacher teacher= new Teacher();
        teacher.setTeacherId(4);
        teacher.setSubject("computer science");
        teacher.setTeacherName("Elon Musk");
        Session session= sessionFactory.openSession();
        session.update(teacher);
        System.out.println("updated.");
        session.beginTransaction().commit();
        session.close();
    }
    public void insert(){
        Teacher teacher= new Teacher();
        teacher.setSubject("computer science");
        teacher.setTeacherName("Elon Musk");
        Session session=sessionFactory.openSession();
        session.save(teacher);
        session.beginTransaction().commit();
        session.close();
    }

    public static void main(String[] args) {
        CrudApp crudApp = new CrudApp();
        crudApp.insert();
        List<Teacher> teacherList= crudApp.getAllTeachers();
        for(Teacher teacher:teacherList){
            System.out.println(teacher);
        }
        crudApp.delete();
        crudApp.update();
    }

}
