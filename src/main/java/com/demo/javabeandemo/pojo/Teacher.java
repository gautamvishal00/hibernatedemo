package com.demo.javabeandemo.pojo;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="test_teacher")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Teacher implements Serializable {
    private static final long serialVersionUID=1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int teacherId;
    @Column(name="teacher_name",length = 30)
    private String teacherName;
    @Column(name="subject",length = 30)
    private String subject;

}
